DELETE FROM N_DOCPUB
WHERE ID IN (
	SELECT ID FROM (SELECT DP.ID FROM N_DOCPUB DP, N_PUB P
						WHERE DP.PUB_ID = P.ID
					    AND P.PRODUCT_ID = @productId
                    ) X
);