#!/usr/bin/env python

# import packages
import os
import sys
import datetime
import time
from urllib3 import *
import requests
import paramiko
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

################################################################################################
# $cd Documents/projects/python-backup-restore/
# $python3 backup-restore.py prod b dev a uspnf 10.13.124.248 10.13.145.138 GdeZOY2z8eXV GdeZOY2z8eXV /Users/sht/Documents/projects/python-backup-restore/ /Users/sht/Documents/santosh/USP/AWSKeys/ > output.log
################################################################################################

if len(sys.argv) != 12:
    print('Usage: python3 backup-restore.py <backup_env_source> <backup_schema_source> <restore_env_destination> <restore_schema_destination> <product> <solr_ip_source> <solr_ip_destination> <solr_password_source> <solr_password_destination> <source_code_folder> <aws_keys_folder>')
    print('For example: python3 backup-restore.py prod a dev a uspnf 10.13.124.248 10.13.145.138 GdeZOY2z8eXV GdeZOY2z8eXV /Users/sht/Documents/projects/python-backup-restore/ /Users/sht/Documents/santosh/USP/AWSKeys/ > output.log')
    sys.exit()

#Global Variables
now = datetime.datetime.now()
backup_env_source = ''
backup_schema_source = ''
restore_env_destination = ''
restore_schema_destination = ''
product = ''
solr_ip_source = ''
solr_ip_destination = ''
solr_password_source = ''
solr_password_destination = ''
source_code_folder = ''
aws_keys_folder = ''
SOLR_HOST_SOURCE = ''
SOLR_HOST_DESTINATION = ''
SOLR_CORE_SOURCE = ''
SOLR_CORE_DESTINATION = ''
RDS_HOST_SOURCE = ''
RDS_HOST_DESTINATION = ''
RDS_SCHEMA_SOURCE = ''
RDS_SCHEMA_DESTINATION = ''
KEYPAIR_SOURCE = ''
KEYPAIR_DESTINATION = ''
SOLR_BACKUP_NAME = ''
SOLR_BACKUP_URL = ''
SOLR_RESTORE_URL = ''
BACKUP_SCRIPT_FOLDER = ''
RESTORE_SCRIPT_FOLDER = ''
DELETE_SCRIPT_FOLDER = ''
OUTPUT_FOLDER = ''
TAR_DIR_REMOTE = '/tmp'
TAR_FILENAME = ''

#Get Command Line Arguments
def getCommandLineArgs():
    global backup_env_source
    backup_env_source = sys.argv[1]
    global backup_schema_source
    backup_schema_source = sys.argv[2]
    global restore_env_destination
    restore_env_destination = sys.argv[3]
    global restore_schema_destination
    restore_schema_destination = sys.argv[4]
    global product
    product = sys.argv[5]
    global solr_ip_source
    solr_ip_source = sys.argv[6]
    global solr_ip_destination
    solr_ip_destination = sys.argv[7]
    global solr_password_source
    solr_password_source = sys.argv[8]
    global solr_password_destination
    solr_password_destination = sys.argv[9]
    global source_code_folder
    source_code_folder = sys.argv[10]
    global aws_keys_folder
    aws_keys_folder = sys.argv[11]

#Prepare SOLR Hostname for Source
def prepareSOLR_HOST_SOURCE():
    global SOLR_HOST_SOURCE
    if backup_env_source == 'dev':
        if product == 'uspnf':
            SOLR_HOST_SOURCE = 'solr.dev.usp-cloud.org'
        elif product == 'usppf':
            SOLR_HOST_SOURCE = 'solr.dev.usp-cloud.org'
        elif product == 'uspdsc':
            SOLR_HOST_SOURCE = 'solr8.dev.usp-cloud.org'
    elif backup_env_source == 'test':
        if product == 'uspnf':
            SOLR_HOST_SOURCE = 'solr.test.usp-cloud.org'
        elif product == 'usppf':
            SOLR_HOST_SOURCE = 'solr.test.usp-cloud.org'
        elif product == 'uspdsc':
            SOLR_HOST_SOURCE = 'solr8.test.usp-cloud.org'
    elif backup_env_source == 'prod':
        if product == 'uspnf':
            SOLR_HOST_SOURCE = 'solrm.usp-cloud.org'
        elif product == 'usppf':
            SOLR_HOST_SOURCE = 'solrm.usp-cloud.org'
        elif product == 'uspdsc':
            SOLR_HOST_SOURCE = 'solr8m.usp-cloud.org'
    print(datetime.datetime.now(), ' SOLR_HOST_SOURCE=', SOLR_HOST_SOURCE )

#Prepare SOLR Hostname for Destination
def prepareSOLR_HOST_DESTINATION():
    global SOLR_HOST_DESTINATION
    if restore_env_destination == 'dev':
        if product == 'uspnf':
            SOLR_HOST_DESTINATION = 'solr.dev.usp-cloud.org'
        elif product == 'usppf':
            SOLR_HOST_DESTINATION = 'solr.dev.usp-cloud.org'
        elif product == 'uspdsc':
            SOLR_HOST_DESTINATION = 'solr8.dev.usp-cloud.org'
    elif restore_env_destination == 'test':
        if product == 'uspnf':
            SOLR_HOST_DESTINATION = 'solr.test.usp-cloud.org'
        elif product == 'usppf':
            SOLR_HOST_DESTINATION = 'solr.test.usp-cloud.org'
        elif product == 'uspdsc':
            SOLR_HOST_DESTINATION = 'solr8.test.usp-cloud.org'
    elif restore_env_destination == 'prod':
        if product == 'uspnf':
            SOLR_HOST_DESTINATION = 'solrm.usp-cloud.org'
        elif product == 'usppf':
            SOLR_HOST_DESTINATION = 'solrm.usp-cloud.org'
        elif product == 'uspdsc':
            SOLR_HOST_DESTINATION = 'solr8m.usp-cloud.org'
    print(datetime.datetime.now(), ' SOLR_HOST_DESTINATION=', SOLR_HOST_DESTINATION )

#Prepare SOLR Core Name for Source
def prepareSOLR_CORE_SOURCE():
    global SOLR_CORE_SOURCE
    if product == 'uspnf':
        if backup_schema_source == 'a':
            if backup_env_source == 'dev':
                SOLR_CORE_SOURCE = 'uspnf_nabud-a-search'
            else:
                SOLR_CORE_SOURCE = 'uspnf_nabu-a-search'
        if backup_schema_source == 'b':
            if backup_env_source == 'dev':
                SOLR_CORE_SOURCE = 'uspnf_nabud-b-search'
            else:
                SOLR_CORE_SOURCE = 'uspnf_nabu-b-search'
    elif product == 'usppf':
        if backup_schema_source == 'a':
            if backup_env_source == 'dev':
                SOLR_CORE_SOURCE = 'usppf_nabud-a-search'
            else:
                SOLR_CORE_SOURCE = 'usppf_nabu-a-search'
        if backup_schema_source == 'b':
            if backup_env_source == 'dev':
                SOLR_CORE_SOURCE = 'usppf_nabud-b-search'
            else:
                SOLR_CORE_SOURCE = 'usppf_nabu-b-search'
    elif product == 'uspdsc':
        if backup_schema_source == 'a':
            SOLR_CORE_SOURCE = 'uspdsc_nabu-a-search'
        if backup_schema_source == 'b':
            SOLR_CORE_SOURCE = 'uspdsc_nabu-b-search'
    print(datetime.datetime.now(), ' SOLR_CORE_SOURCE=', SOLR_CORE_SOURCE )

#Prepare SOLR Core Name for Destination
def prepareSOLR_CORE_DESTINATION():
    global SOLR_CORE_DESTINATION
    if product == 'uspnf':
        if restore_schema_destination == 'a':
            if restore_env_destination == 'dev':
                SOLR_CORE_DESTINATION = 'uspnf_nabud-a-search'
            else:
                SOLR_CORE_DESTINATION = 'uspnf_nabu-a-search'
        if restore_schema_destination == 'b':
            if restore_env_destination == 'dev':
                SOLR_CORE_DESTINATION = 'uspnf_nabud-b-search'
            else:
                SOLR_CORE_DESTINATION = 'uspnf_nabu-b-search'
    elif product == 'usppf':
        if restore_schema_destination == 'a':
            if restore_env_destination == 'dev':
                SOLR_CORE_DESTINATION = 'usppf_nabud-a-search'
            else:
                SOLR_CORE_DESTINATION = 'usppf_nabu-a-search'
        if restore_schema_destination == 'b':
            if restore_env_destination == 'dev':
                SOLR_CORE_DESTINATION = 'usppf_nabud-b-search'
            else:
                SOLR_CORE_DESTINATION = 'usppf_nabu-b-search'
    elif product == 'uspdsc':
        if restore_schema_destination == 'a':
            SOLR_CORE_DESTINATION = 'uspdsc_nabu-a-search'
        if restore_schema_destination == 'b':
            SOLR_CORE_DESTINATION = 'uspdsc_nabu-b-search'
    print(datetime.datetime.now(), ' SOLR_CORE_DESTINATION=', SOLR_CORE_DESTINATION )

#Prepare RDS Host Name for Source
def prepareRDS_HOST_SOURCE():
    global RDS_HOST_SOURCE
    if backup_env_source == 'dev':
        RDS_HOST_SOURCE = 'rds.dev.usp-cloud.org'
    elif backup_env_source == 'test':
        RDS_HOST_SOURCE = 'rds.test.usp-cloud.org'
    elif backup_env_source == 'prod':
        RDS_HOST_SOURCE = 'rdsreader.usp-cloud.org'
    print(datetime.datetime.now(), ' RDS_HOST_SOURCE=', RDS_HOST_SOURCE )

#Prepare RDS Host Name for Destination
def prepareRDS_HOST_DESTINATION():
    global RDS_HOST_DESTINATION
    if restore_env_destination == 'dev':
        RDS_HOST_DESTINATION = 'rds.dev.usp-cloud.org'
    elif restore_env_destination == 'test':
        RDS_HOST_DESTINATION = 'rds.test.usp-cloud.org'
    elif restore_env_destination == 'prod':
        RDS_HOST_DESTINATION = 'rds.usp-cloud.org'
    print(datetime.datetime.now(), ' RDS_HOST_DESTINATION=', RDS_HOST_DESTINATION )

#Prepare RDS Schema Name for Source
def prepareRDS_SCHEMA_SOURCE():
    global RDS_SCHEMA_SOURCE
    if backup_schema_source == 'a':
        RDS_SCHEMA_SOURCE = 'uspnf_nabudoca'
    elif backup_schema_source == 'b':
        RDS_SCHEMA_SOURCE = 'uspnf_nabudocb'
    print(datetime.datetime.now(), ' RDS_SCHEMA_SOURCE=', RDS_SCHEMA_SOURCE )

#Prepare RDS Schema Name for Destination
def prepareRDS_SCHEMA_DESTINATION():
    global RDS_SCHEMA_DESTINATION
    if restore_schema_destination == 'a':
        RDS_SCHEMA_DESTINATION = 'uspnf_nabudoca'
    elif restore_schema_destination == 'b':
        RDS_SCHEMA_DESTINATION = 'uspnf_nabudocb'
    print(datetime.datetime.now(), ' RDS_SCHEMA_DESTINATION=', RDS_SCHEMA_DESTINATION )

#Prepare KeyPair Name for Source
def prepareKEYPAIR_SOURCE():
    global KEYPAIR_SOURCE
    if backup_env_source == 'dev':
        KEYPAIR_SOURCE = aws_keys_folder + 'usp_tf_dev.pem'
    elif backup_env_source == 'test':
        KEYPAIR_SOURCE = aws_keys_folder + 'usp_tf_test.pem'
    elif backup_env_source == 'prod':
        KEYPAIR_SOURCE = aws_keys_folder + 'usp-virginia.pem'

#Prepare KeyPair Name for Destination
def prepareKEYPAIR_DESTINATION():
    global KEYPAIR_DESTINATION
    if restore_env_destination == 'dev':
        KEYPAIR_DESTINATION = aws_keys_folder + 'usp_tf_dev.pem'
    elif restore_env_destination == 'test':
        KEYPAIR_DESTINATION = aws_keys_folder + 'usp_tf_test.pem'
    elif restore_env_destination == 'prod':
        KEYPAIR_DESTINATION = aws_keys_folder + 'usp-virginia.pem'

#Prepare SOLR backup and restore URLs, TAR filename
def prepareVariables():
    global SOLR_BACKUP_NAME
    SOLR_BACKUP_NAME = str(now.year) + '_' + str(now.month) + '_' + str(now.day) + '_' + str(now.hour) + '_' + str(now.minute) + '_' + SOLR_CORE_SOURCE
    global SOLR_BACKUP_URL
    SOLR_BACKUP_URL = 'http://' + SOLR_HOST_SOURCE + '/solr/' + SOLR_CORE_SOURCE + '/replication?command=backup&name=' + SOLR_BACKUP_NAME + '&location=/tmp'
    print(datetime.datetime.now(), ' SOLR_BACKUP_URL=', SOLR_BACKUP_URL)
    global SOLR_RESTORE_URL
    SOLR_RESTORE_URL = 'http://' + SOLR_HOST_DESTINATION + '/solr/' + SOLR_CORE_DESTINATION + '/replication?command=restore&name=' + SOLR_BACKUP_NAME + '&location=/tmp'
    print(datetime.datetime.now(), ' SOLR_RESTORE_URL=', SOLR_RESTORE_URL)
    global TAR_FILENAME
    TAR_FILENAME = 'solr-' + SOLR_BACKUP_NAME + '.tar'
    print(datetime.datetime.now(), ' TAR_FILENAME=', TAR_FILENAME)
    global BACKUP_SCRIPT_FOLDER
    BACKUP_SCRIPT_FOLDER = source_code_folder + 'rds-backup'
    global RESTORE_SCRIPT_FOLDER
    RESTORE_SCRIPT_FOLDER = source_code_folder + 'rds-restore'
    global DELETE_SCRIPT_FOLDER
    DELETE_SCRIPT_FOLDER = source_code_folder + 'rds-delete'
    global OUTPUT_FOLDER
    OUTPUT_FOLDER = source_code_folder + 'output'

#Delete all files from 'output' folder on Developer machine
def cleanOutputFolder():
    try:
        os.chdir(OUTPUT_FOLDER)
        cmd = 'rm -fR *'
        os.system(cmd)
        print(datetime.datetime.now(), 'Deleted all files in output folder')
    except Exception as e:
        print('Failed in cleanOutputFolder()')
        print(e)
        sendMail('Backup Restore Job Failed in cleanOutputFolder()')
        sys.exit()
    
#create backup from SOLR Source
def backupSolrSource():
    try:
        headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
        params = (
            ('user', 'user:' + solr_password_source),
        )
        response = requests.get(SOLR_BACKUP_URL, params=params)
        if response.status_code == 200:
            print(datetime.datetime.now(), ' SOLR backup on Source Successful !')
        else:
            print(datetime.datetime.now(), ' SOLR backup on Source Failed !')
        #Sleep for 30 seconds
        time.sleep(30)
    except Exception as e:
        print('Failed in backupSolrSource()')
        print(e)
        sendMail('Backup Restore Job Failed in backupSolrSource()')
        sys.exit()

#Generate Insert SQL scripts from Source RDS
def backupRDSSource():
    try:
        os.chdir(BACKUP_SCRIPT_FOLDER)
        
        cmd = "echo 'START TRANSACTION;' > " + OUTPUT_FOLDER + "/RESTORE_N_PUB.sql"
        os.system(cmd)
        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_SOURCE + " -P 3306 -N -D " + RDS_SCHEMA_SOURCE + " -e \"set @productId=1; set @restoreschema='" + RDS_SCHEMA_DESTINATION + "'; source N_PUB.sql;\" >> " + OUTPUT_FOLDER + "/RESTORE_N_PUB.sql"
        os.system(cmd)
        cmd = "echo 'COMMIT;' >> " + OUTPUT_FOLDER + "/RESTORE_N_PUB.sql"
        os.system(cmd)
        print(datetime.datetime.now(), ' Created backup script for N_PUB')

        cmd = "echo 'START TRANSACTION;' > " + OUTPUT_FOLDER + "/RESTORE_N_DOC.sql"
        os.system(cmd)
        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_SOURCE + " -P 3306 -N -D " + RDS_SCHEMA_SOURCE + " -e \"set @productId=1; set @restoreschema='" + RDS_SCHEMA_DESTINATION + "'; source N_DOC.sql;\" >> " + OUTPUT_FOLDER + "/RESTORE_N_DOC.sql"
        os.system(cmd)
        cmd = "echo 'COMMIT;' >> " + OUTPUT_FOLDER + "/RESTORE_N_DOC.sql"
        os.system(cmd)
        print(datetime.datetime.now(), ' Created backup script for N_DOC')

        cmd = "echo 'START TRANSACTION;' > " + OUTPUT_FOLDER + "/RESTORE_N_DOCPUB_BACKUP.sql"
        os.system(cmd)
        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_SOURCE + " -P 3306 -N -D " + RDS_SCHEMA_SOURCE + " -e \"set @productId=1; set @restoreschema='" + RDS_SCHEMA_DESTINATION + "'; source N_DOCPUB_BACKUP.sql;\" >> " + OUTPUT_FOLDER + "/RESTORE_N_DOCPUB_BACKUP.sql"
        os.system(cmd)
        cmd = "echo 'COMMIT;' >> " + OUTPUT_FOLDER + "/RESTORE_N_DOCPUB_BACKUP.sql"
        os.system(cmd)
        print(datetime.datetime.now(), ' Created backup script for N_DOCPUB_BACKUP')
    except Exception as e:
        print('Failed in backupRDSSource()')
        print(e)
        sendMail('Backup Restore Job Failed in backupRDSSource()')
        sys.exit()

#Create TAR file from SOLR backup on Source and download it on Developer machine
def createSolrTarSource():
    try:
        if backup_env_source != restore_env_destination:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(solr_ip_source, port=22, username="bitnami", key_filename=KEYPAIR_SOURCE)
            cmd = 'cd ' + TAR_DIR_REMOTE + '; tar cvf ' + TAR_FILENAME + ' snapshot.' + SOLR_BACKUP_NAME
            stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
            print(datetime.datetime.now(), ' SOLR TAR created on Source server')
            #Sleep for 30 seconds
            time.sleep(30)

            sftp = client.open_sftp()
            sftp.get(TAR_DIR_REMOTE + '/' + TAR_FILENAME, OUTPUT_FOLDER + '/' + TAR_FILENAME)
            sftp.close()
            client.close()
            print(datetime.datetime.now(), ' SOLR TAR received via SFTP on developer machine')
    except Exception as e:
        print('Failed in createSolrTarSource()')
        print(e)
        sendMail('Backup Restore Job Failed in createSolrTarSource()')
        sys.exit()

#Delete all data from SOLR Destination
def deleteSolrDestination():
    try:
        headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
        params = (
            ('user', 'user:' + solr_password_destination),
        )
        SOLR_DELETE_URL = 'http://' + SOLR_HOST_DESTINATION + '/solr/' + SOLR_CORE_DESTINATION + '/update?stream.body=<delete><query>*:*</query></delete>&commit=true'
        print(datetime.datetime.now(), 'SOLR_DELETE_URL=', SOLR_DELETE_URL)
        response = requests.get(SOLR_DELETE_URL, params=params)
        if response.status_code == 200:
            print(datetime.datetime.now(), ' SOLR Delete on Destination Successful !')
        else:
            print(datetime.datetime.now(), ' SOLR Delete on Destination Failed !')
        #Sleep for 30 seconds
        time.sleep(30)
    except Exception as e:
        print('Failed in deleteSolrDestination()')
        print(e)
        sendMail('Backup Restore Job Failed in deleteSolrDestination()')
        sys.exit()

#Delete all data from RDS Destination
def deleteRDSDestination():    
    try:
        os.chdir(DELETE_SCRIPT_FOLDER)
        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_DESTINATION + " -P 3306 -N -D " + RDS_SCHEMA_DESTINATION + " -e \"set @productId=1; source DELETE_N_DOCPUB.sql;\" " 
        os.system(cmd)
        print(datetime.datetime.now(), ' Deleted data from N_DOCPUB in Destination RDS')
        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_DESTINATION + " -P 3306 -N -D " + RDS_SCHEMA_DESTINATION + " -e \"set @productId=1; source DELETE_N_DOC.sql;\" " 
        os.system(cmd)
        print(datetime.datetime.now(), ' Deleted data from N_DOC in Destination RDS')
        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_DESTINATION + " -P 3306 -N -D " + RDS_SCHEMA_DESTINATION + " -e \"set @productId=1; source DELETE_N_PUB.sql;\" " 
        os.system(cmd)
        print(datetime.datetime.now(), ' Deleted data from N_PUB in Destination RDS')
    except Exception as e:
        print('Failed in deleteRDSDestination()')
        print(e)
        sendMail('Backup Restore Job Failed in deleteRDSDestination()')
        sys.exit()

#Upload TAR file to Source Destination and extract it there
def extractSolrTarDestination():
    try:
        if backup_env_source != restore_env_destination:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(solr_ip_destination, port=22, username="bitnami", key_filename=KEYPAIR_DESTINATION)
            
            sftp = client.open_sftp()
            sftp.put(OUTPUT_FOLDER + '/' + TAR_FILENAME, TAR_DIR_REMOTE + '/' + TAR_FILENAME)
            sftp.close()
            print(datetime.datetime.now(), ' TAR uploaded via SFTP to SOLR Destination server')

            cmd = 'cd ' + TAR_DIR_REMOTE + '; tar xvf ' + TAR_FILENAME 
            stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
            #Sleep for 60 seconds
            time.sleep(60)
            client.close()
            print(datetime.datetime.now(), ' TAR extracted on SOLR Destination server')
    except Exception as e:
        print('Failed in extractSolrTarDestination()')
        print(e)
        sendMail('Backup Restore Job Failed in extractSolrTarDestination()')
        sys.exit()

#Restore data on SOLR Destination
def restoreSolrDestination():
    try:
        headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
        params = (
            ('user', 'user:' + solr_password_destination),
        )
        response = requests.get(SOLR_RESTORE_URL, params=params)
        if response.status_code == 200:
            print(datetime.datetime.now(), ' SOLR Restore on Destination Successful !')
        else:
            print(datetime.datetime.now(), ' SOLR Restore on Destination Failed !')
    except Exception as e:
        print('Failed in restoreSolrDestination()')
        print(e)
        sendMail('Backup Restore Job Failed in restoreSolrDestination()')
        sys.exit()

#Restore data on RDS Destination
def restoreRDSDestination():
    try:
        os.chdir(RESTORE_SCRIPT_FOLDER)
        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_DESTINATION + " -P 3306 -N -D " + RDS_SCHEMA_DESTINATION + " -e \"set @productId=1; source BEFORE_N_DOCPUB.sql;\" " 
        os.system(cmd)
        print(datetime.datetime.now(), ' Dropped and Created N_DOCPUB_BACKUP in RDS Destination schema')			

        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_DESTINATION + " -P 3306 -N -D " + RDS_SCHEMA_SOURCE + " -e \"set @productId=1; source " + OUTPUT_FOLDER + "/RESTORE_N_PUB.sql;\" " 
        os.system(cmd)
        print(datetime.datetime.now(), ' Restored N_PUB in RDS Destination schema')			
        
        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_DESTINATION + " -P 3306 -N -D " + RDS_SCHEMA_SOURCE + " -e \"set @productId=1; source " + OUTPUT_FOLDER + "/RESTORE_N_DOC.sql;\" " 
        os.system(cmd)
        print(datetime.datetime.now(), ' Restored N_DOC in RDS Destination schema')			
        
        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_DESTINATION + " -P 3306 -N -D " + RDS_SCHEMA_SOURCE + " -e \"set @productId=1; source " + OUTPUT_FOLDER + "/RESTORE_N_DOCPUB_BACKUP.sql;\" " 
        os.system(cmd)
        print(datetime.datetime.now(), ' Restored N_DOCPUB_BACKUP in RDS Destination schema')			
        
        cmd = "echo 'START TRANSACTION;' > " + OUTPUT_FOLDER + "/RESTORE_N_DOCPUB.sql"
        os.system(cmd)
        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_DESTINATION + " -P 3306 -N -D " + RDS_SCHEMA_DESTINATION + " -e \"set @productId=1; set @restoreschema='" + RDS_SCHEMA_DESTINATION + "'; source N_DOCPUB.sql;\" >> " + OUTPUT_FOLDER + "/RESTORE_N_DOCPUB.sql" 
        os.system(cmd)
        cmd = "echo 'COMMIT;' >> " + OUTPUT_FOLDER + "/RESTORE_N_DOCPUB.sql"
        os.system(cmd)
        print(datetime.datetime.now(), ' Created RESTORE_N_DOCPUB script')			
        
        cmd = "mysql -u uspnf --password=uspnfaccess -h " + RDS_HOST_DESTINATION + " -P 3306 -N -D " + RDS_SCHEMA_DESTINATION + " -e \"source " + OUTPUT_FOLDER + "/RESTORE_N_DOCPUB.sql;\" " 
        os.system(cmd)
        print(datetime.datetime.now(), ' Restored N_DOCPUB in RDS Destination schema')	
    except Exception as e:
        print('Failed in restoreRDSDestination()')
        print(e)
        sendMail('Backup Restore Job Failed in restoreRDSDestination()')
        sys.exit()

#Delete TAR file and snapshot folder on both SOLR Source and SOLR Destination
def cleanupSolr():    
    try:    
        # Cleanup Solr SOURCE server
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(solr_ip_source, port=22, username="bitnami", key_filename=KEYPAIR_SOURCE)

        cmd = 'rm -f ' + TAR_DIR_REMOTE + '/' + TAR_FILENAME 
        stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
        cmd = 'sudo rm -fR ' + TAR_DIR_REMOTE + '/snapshot.' + SOLR_BACKUP_NAME 
        stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
        cmd = 'exit'
        stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
        client.close()
        print(datetime.datetime.now(), ' SOLR TAR and snapshot folder deleted on Source server')
        
        #Cleanup Solr DESTINATION server
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(solr_ip_destination, port=22, username="bitnami", key_filename=KEYPAIR_DESTINATION)

        cmd = 'rm -f ' + TAR_DIR_REMOTE + '/' + TAR_FILENAME 
        stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
        cmd = 'rm -fR ' + TAR_DIR_REMOTE + '/snapshot.' + SOLR_BACKUP_NAME 
        stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
        cmd = 'exit'
        stdin, stdout, stderr = client.exec_command(cmd, get_pty=True)
        client.close()
        print(datetime.datetime.now(), ' SOLR TAR and snapshot folder deleted on Destination server')
    except Exception as e:
        print('Failed in cleanupSolr()')
        print(e)
        sendMail('Backup Restore Job Failed in cleanupSolr()')
        sys.exit()

#Send Email to Developer
def sendMail(bodycontent):
    try:
        fromaddr = "sht@usp.org"
        toaddr = "sht@usp.org"
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = "Backup Restore Job"
        #body = "Backup Restore Job Complete !!"
        msg.attach(MIMEText(bodycontent, 'plain'))
        server = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com', 587)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login("AKIAJUWVYEDKCBAWMEQQ", "Au0s1e3ZrpkM2Sn9PWzdNCI07cVk+88srBp41ReWTcm3")
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        print(datetime.datetime.now(), 'Sent Email to Developer')
    except Exception as e:
        print('Failed in sendMail()')
        print(e)
        sys.exit()

#MAIN function to start the process
def process():
    #Get Command Line Arguments
    getCommandLineArgs()
    
    #Prepare SOLR Hostname for Source
    prepareSOLR_HOST_SOURCE()
    
    #Prepare SOLR Hostname for Destination
    prepareSOLR_HOST_DESTINATION()
    
    #Prepare SOLR Core Name for Source
    prepareSOLR_CORE_SOURCE()
    
    #Prepare SOLR Core Name for Destination
    prepareSOLR_CORE_DESTINATION()
    
    #Prepare RDS Host Name for Source
    prepareRDS_HOST_SOURCE()
    
    #Prepare RDS Host Name for Destination
    prepareRDS_HOST_DESTINATION()
    
    #Prepare RDS Schema Name for Source
    prepareRDS_SCHEMA_SOURCE()
    
    #Prepare RDS Schema Name for Destination
    prepareRDS_SCHEMA_DESTINATION()
    
    #Prepare KeyPair Name for Source
    prepareKEYPAIR_SOURCE()
    
    #Prepare KeyPair Name for Destination
    prepareKEYPAIR_DESTINATION()
    
    #Prepare SOLR backup and restore URLs, TAR filename
    prepareVariables()
    
    #Delete all files from 'output' folder on Developer machine
    cleanOutputFolder()
    
    #create backup from SOLR Source
    backupSolrSource()
    
    #Generate Insert SQL scripts from Source RDS
    backupRDSSource()
    
    #Create TAR file from the SOLR backup on Source and download it on Developer machine
    createSolrTarSource()
    
    #Delete all data from SOLR Destination
    deleteSolrDestination()
    
    #Delete all data from RDS Destination
    deleteRDSDestination()
    
    #Upload TAR file to Source Destination and extract it there
    extractSolrTarDestination()
    
    #Restore data on SOLR Destination
    restoreSolrDestination()
    
    #Restore data on RDS Destination
    restoreRDSDestination()
    
    #Delete TAR file and snapshot folder on both SOLR Source and SOLR Destination
    cleanupSolr()
    
    #Send Email to Developer
    sendMail('Backup Restore Job Complete !!')

#Invoke the main function to start the process
process()
