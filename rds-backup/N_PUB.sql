SELECT CONCAT('INSERT INTO ',
@restoreschema,
'.N_PUB(PUBTYPE_ID, PUBSCOPE_ID, PUBSTATUS_ID, LANGUAGE_ID, NAME, DESCRIPTION, CREATION_DATE, MODIFIED_DATE, PUBLISH_DATE, SHOW_DATE, HIDE_DATE, TOC, PRODUCT_ID) VALUES (',
'\'', PUBTYPE_ID, '\',',
'\'', PUBSCOPE_ID, '\',',
'\'', PUBSTATUS_ID, '\',',
'\'', LANGUAGE_ID, '\',',
'\'', NAME, '\',',
IF(DESCRIPTION IS NOT NULL, CONCAT('\'', DESCRIPTION, '\','),'NULL,'),
'\'', CREATION_DATE, '\',',
'\'', MODIFIED_DATE, '\',',
IF(PUBLISH_DATE IS NOT NULL, CONCAT('\'', PUBLISH_DATE, '\','),'NULL,'),
IF(SHOW_DATE IS NOT NULL, CONCAT('\'', SHOW_DATE, '\','),'NULL,'),
IF(HIDE_DATE IS NOT NULL, CONCAT('\'', HIDE_DATE, '\','),'NULL,'),
IF(TOC IS NOT NULL, CONCAT('\'', TOC, '\','),'NULL,'),
'\'', PRODUCT_ID, '\'',
');') HEADER_TO_BE_REMOVED
FROM N_PUB
WHERE PRODUCT_ID = @productId;
